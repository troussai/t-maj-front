export default {
  vue_titles: {
    add_field: 'Définition d\'un champ'
  },
  labels: {
    my_account: 'Mon compte',
    log_out: 'Se déconnecter',
    sign_in: 'Se connecter',
    sign_up: 'S\'inscrire',
    mail_address: 'Addresse mail',
    password: 'Mot de passe',
    password_length: 'Longueure de mot de passe',
    password_uppercase: 'Le mot de passe contient une majuscule',
    password_number: 'Le mot de passe contient un nombre',
    password_special: 'Le mot de passe contient un caractère spécial',
    login: 'Se connecter',
    validate: 'Valider',
    cancel: 'Annuler',
    first_name: 'Votre prénom',
    last_name: 'Votre nom',
    ok: 'Ok',
    field_name: 'Nom du champ'
  },
  actions: {
    get_chart: 'Statistiques',
    admin_panel: 'Administration'
  },
  errors: {
    title: 'Attention',
    enter_mail: 'Entrez une addresse mail au format correct',
    password_different: 'Les mots de passe sont différents',
    mail_already_use: 'Cette addresse mail est déjà utilisée',
    invalid_credential: 'Mauvais login / mot de passe',
    invalid_token: 'La session a expirée',
    internal_error: 'Un erreur est survenue'
  },
  hints: {
    enter_password: 'Entrez votre mot de passe',
    enter_mail_address: 'Entrez votre addresse mail',
    enter_first_name: 'Entrez votre prénom',
    enter_last_name: 'Entrez votre nom de famille',
    password_confirm: 'Confirmez votre mot de passe',
    enter_field_name: 'Entrez le nom du champ',
    humidity_level: 'Taux d\'humidité du champ',
    temperature_level: 'Taux de température du champ'
  },
  messages: {
    account_created: 'Votre compte a bien été crée',
    must_be_logged: 'Vous devez être connectés pour accéder à cette fonctionnalitée'
  },
  page_title: {
    menu: 'Menu'
  }
}
