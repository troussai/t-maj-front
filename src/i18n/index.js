import enUS from './en-us'
import fr from './fr-fr'

export default {
  'en-us': enUS,
  'fr-fr': fr
}