// Import pages and layouts
import LoggedLayout from 'layouts/LoggedLayout'
import UnloggedLayout from 'layouts/UnloggedLayout'
import PageIndex from 'pages/Index'
import SignUpPage from 'pages/Auth/SignUpPage'
import SignInPage from 'pages/Auth/SignInPage'
import AdminPage from 'pages/AdminPage'

const routes = [
  {
    path: '/app',
    component: LoggedLayout,
    children: [
      { path: '', component: PageIndex },
      { path: 'admin', component: AdminPage }
    ]
  },
  {
    path: '/',
    component: UnloggedLayout,
    children: [
      { path: '', component: SignInPage},
      { path: '/sign-up', component: SignUpPage}
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
